import React from "react";
import renderer from "react-test-renderer";
import { renderHook, act } from "@testing-library/react-hooks";
import useWeather from "../use-weather";

test("Testing react custom hooks", () => {
  const result = renderHook(() => useWeather());
  expect(result.result.current.data).toBeTruthy();
  expect(result.result.current.loading).toBeFalsy();
  expect(result.result.current.isError).toBeTruthy();
});
