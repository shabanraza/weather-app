import React from "react";
import renderer from "react-test-renderer";
import useCity from "../use-city";
import { renderHook, act } from "@testing-library/react-hooks";

test("Testing react custom hooks", () => {
  const result = renderHook(() => useCity("London"));
  expect(result.result.current.data).toBeTruthy();
  expect(result.result.current.loading).toBeFalsy();
  expect(result.result.current.isError).toBeTruthy();
});
