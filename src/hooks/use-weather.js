import { useState, useEffect } from "react";

const cities = ["Barcelona", "Helsinki", "Florence", "Moscow", "london"];

const getUrl = (city) => {
  return `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=3d8b309701a13f65b660fa2c64cdc517`;
};

export default function useWeather() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      try {
        const res = await Promise.all(
          cities.map((city) => fetch(getUrl(city)))
        );
        const data = await Promise.all(res.map((resp) => resp.json()));
        setData(data);
        setLoading(false);
      } catch (error) {
        console.error(error.messsage);
        setLoading(false);
        setIsError(true);
      }
    }
    fetchData();
  }, []);

  return {
    data: data,
    loading: loading,
    isError: isError,
  };
}
