import { useState, useEffect } from "react";

const getUrl = (city) => {
  return `http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=3d8b309701a13f65b660fa2c64cdc517`;
};

export default function useCity(city) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      try {
        const res = await fetch(getUrl(city));
        const data = await res.json();
        const arr = data?.list.filter(
          (el) => new Date(el.dt_txt).getHours() === 9
        );
        setData(arr);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setIsError(true);
      }
    }
    fetchData();
    return ()=>{
      setData([])
    }
  }, [city]);

  return {
    data: data,
    loading: loading,
    isError: isError,
  };
}
