import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from './components/Home';
import WeatherDetail from './components/WeatherDetail'


ReactDOM.render(
  <Router>
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route  path="/:city"  >
        <WeatherDetail />
      </Route>
    </Switch>
  </Router>,
  document.getElementById("root")
);
