import React from "react";
import "./index.scss";
import moment from "moment-timezone";
import { Link } from "react-router-dom";
export default function WeatherCards({
  city: {
    name,
    timezone,
    weather,
    main: { temp },
    sys: { sunrise, sunset },
  },
}) {
  const sunriseTime = moment
    .unix(sunrise * 1000 + timezone * 1000)
    .format("h:mm:ss A");
  const sunsetTime = moment
    .unix(sunset + 3600000 * timezone)
    .format("h:mm:ss A");
  const icon = `https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${weather[0]["icon"]}.svg`;

  return (
    <Link to={`/${name}`}>
      <div className="card-wrapper">
        <div className="city-wrapper">
          <div className="city-name">
            <h4>{name}</h4>
            <h3>
              {temp} <sup>°C</sup>
            </h3>
          </div>
          <div className="city-icon">
            <figure>
              <img className="city-icon" src={icon} alt={"icon"} />
            </figure>
          </div>
        </div>

        <div className="sun">
          <div className="suntime">
            <span className="title">Sunrise</span>
            <span>{sunriseTime}</span>
          </div>
          <div className="suntime">
            <span className="title">Sunset</span>
            <span>{sunsetTime}</span>
          </div>
        </div>
      </div>
    </Link>
  );
}
