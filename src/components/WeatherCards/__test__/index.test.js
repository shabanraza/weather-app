/**
 * @jest-environment jsdom
 */

import React from "react";
import renderer from "react-test-renderer";
import WeatherCards from "../index.js";
import { BrowserRouter as Router } from "react-router-dom";

test("render weather card component", () => {
  const component = renderer.create(
    <Router>
      <WeatherCards
        city={{
          name: "London",
          timezone: 7200,
          weather: [{ icon: "10d" }],
          main: {
            temp: 23,
          },
          sys: {
            sunrise: new Date().getTime(),
            sunset: new Date().getTime(),
          },
        }}
      />
    </Router>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
