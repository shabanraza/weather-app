/**
 * @jest-environment jsdom
 */

import React from "react";
import renderer from "react-test-renderer";
import WeatherDetail from "../index";
import { BrowserRouter as Router } from "react-router-dom";

test("render weather detail component and show sea level", () => {
  const component = renderer.create(<Router><WeatherDetail /></Router>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
