/**
 * @jest-environment jsdom
 */

 import React from "react";
 import renderer from "react-test-renderer";
 import { BrowserRouter as Router } from "react-router-dom";
 import SeaLevel from '../seaLevel';

 test("render SeaLevel component and show sea level", () => {
   const component = renderer.create(<SeaLevel
    seaData={{
        main: { sea_level:13232, temp :12}
      }}

    
   
   />);
   let tree = component.toJSON();
   expect(tree).toMatchSnapshot();
 });
 