import React from "react";
import "./index.scss";
import useCity from "./../../hooks/use-city";
import { useParams } from "react-router-dom";
import SeaLevel from "./seaLevel";



const  WeatherDetail = React.memo(()=> {
  let { city } = useParams();
  const { data ,loading} = useCity(city);
  if(loading){
      return "Fetching..."
  }
  return (
    <div className="weather-detail">
      <h1>{city}</h1>
      <div className='city-grid'>
      {data.map((seaData, index) => (
        <SeaLevel seaData={seaData} key={index} />
      ))}
      </div>
    </div>
  );
})
export default WeatherDetail;
