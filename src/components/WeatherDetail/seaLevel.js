import React from "react";
import "./index.scss";
import moment from "moment";
import useCity from "./../../hooks/use-city";
import { useParams } from "react-router-dom";

export default function SeaLevel({
  seaData: {
    main: { sea_level, temp }
  },
}) {
  return (
    <div className="sea-wrapper">
      <h3>
        {temp}
        <sup>°C</sup>
      </h3>
    

      <div className="sea-level">
        <h4> Sea Level</h4>
        <span>{sea_level}</span>
      </div>
    </div>
  );
}
