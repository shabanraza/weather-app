import React from 'react';
import renderer from 'react-test-renderer';
import Home from './index.js'

test('render component', async() => {
    const component = renderer.create(
     <Home/>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });