import React from "react";
import useWeather from "./../../hooks/use-weather";
import WeatherCard from "./../WeatherCards";
import './index.scss'
export default function Home() {
  const { data, loading,isError } = useWeather();

  if (loading) {
    return "Fetching...";
  }
 
  return (
    <div className="container">
      <h1>Weather App</h1>
      <div className="Wrapper-cities">
        {data && data.map((el, indx) => <WeatherCard key={indx} city={el} />)}
      </div>
    </div>
  );
}
